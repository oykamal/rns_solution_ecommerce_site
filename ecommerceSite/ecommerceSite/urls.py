from django.contrib import admin
from django.urls import path, include


from graphene_django.views import GraphQLView
from .schema import schema



urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('user.urls')),
    path('',include('product.urls')),
    path('graphql/', GraphQLView.as_view(graphiql=True)),

    # path('',include('customer.urls')),
]
