from django.db import models

# Create your models here.
class Category(models.Model):
    name=models.CharField(max_length=20)
    description = models.TextField()

    def __str__(self):
        return self.name


class Product(models.Model):
    name=models.CharField(max_length=10)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=0)
    stock=models.DecimalField(max_digits=7, decimal_places=0)
    category=models.OneToOneField(Category, on_delete=models.CASCADE,null=True)
    
    def __str__(self):
        return self.name