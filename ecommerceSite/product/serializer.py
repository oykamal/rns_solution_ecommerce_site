from rest_framework import serializers
from .models import Product, Category
from rest_framework.serializers import SerializerMethodField


class CategorySerialzier(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields='__all__'
        

class ProductSerializer(serializers.ModelSerializer):
    category=CategorySerialzier()
    class Meta:
        model=Product
        fields=['name','description','price','stock','category']
        
    # def create(self, validated_data):
    #     category_data = validated_data.pop('category')
    #     category=CategorySerialzier.create(CategorySerialzier(), validated_data=category_data)
    #     product= Product.objects.create(name=validated_data.get('name'),
    #                               price=validated_data.get('price'),
    #                               stock=validated_data.get('stock'),
    #                               description=validated_data.get('description'),
    #                               category=category)
    #     return product 
    