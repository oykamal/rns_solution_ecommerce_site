from rest_framework import routers
from django.urls import path, include
from . import views
# from graphene_django.views import GraphQLView
# from .schema import schema

router= routers.DefaultRouter()


router.register('category', views.CategoryViewset)
router.register('product', views.ProductViewset)
# router.register('supplier', views.SupplierViewset)
# router.register('ssp', views.SupplierSupplyProductViewset)
# router.register('sspe',views.SupplierSupplyProductEditViewset)


urlpatterns = [
    path('', include(router.urls)),
    # path('graphql/', GraphQLView.as_view(schema=schema, graphiql=True)),
]


