from django.shortcuts import render

# Create your views here.
from rest_framework import status, viewsets
from product.models import  Category, Product
from product.serializer import CategorySerialzier, ProductSerializer
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.db.models import Avg, Max, Min, Sum

class CategoryViewset(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerialzier

    
class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
