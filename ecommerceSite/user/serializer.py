
from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import make_password




class UserSerializer(serializers.ModelSerializer):
    """
    use serializer
    
    """
    class Meta:
        model=User
        fields=['id','username','email','first_name','last_name','phone_no','password']
        
    def create(self, validated_data):
        
        user= User.objects.create(email=validated_data.get('email'),
                                  username=validated_data.get('username'),
                                  password=make_password(validated_data.get('password')),
                                  first_name=validated_data.get('first_name'),
                                  last_name=validated_data.get('last_name')
                                  )
        return user
        

    def update(self, instance, validated_data):
        instance.username = validated_data.get("username", instance.username)
        instance.email = validated_data.get("email", instance.email)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.is_active = validated_data.get("is_active", instance.is_active)

        instance.save()

        return instance