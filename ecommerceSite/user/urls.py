from django.urls import path , include
from rest_framework.routers import DefaultRouter
from . import views
from django.conf.urls import url

router=DefaultRouter()

router.register('signup', views.UserSigninViewset, basename='signup')
router.register('user', views.UserViewset)

urlpatterns = [
    path('',include(router.urls)),
    path('login/',views.UserLoginApiView.as_view()),
]

