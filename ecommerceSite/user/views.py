from django.shortcuts import render
from rest_framework import request, status, viewsets
from .models import  User 
from .serializer import  UserSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.response import Response

# Create your views here.
class UserViewset(viewsets.ModelViewSet):
    """handle user CRUD"""
    queryset=User.objects.all()
    serializer_class=UserSerializer
    
    
class UserSigninViewset(viewsets.ViewSet):
    """handle anyone can singup as a user"""
    # permission_classes=[AllowAny,]
    def create(self, request):
        # permission_classes=[IsAuthenticated,]
    
        serializer=UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    
    
    
class UserLoginApiView(ObtainAuthToken):
    """handl creating user authentication tokens"""
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
    